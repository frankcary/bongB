
// shape canvas width
export const WIDTH = 50;

// shape canvas height
export const HEIGHT = 50;

// the number of sites
export const MIN_RANDOM_SITES = 2;
