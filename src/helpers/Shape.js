import Voronoi from './Voronoi';
import {
    WIDTH,
    HEIGHT,
    MIN_RANDOM_SITES
} from './CONSTANT';

class Shape {
    constructor() {
        this.voronoi = new Voronoi();
        this.result = [];
    }


    // 获取形状
    get() {

        if (this.result.length === 0) {
            this.refresh();
        }

        return this.result.shift();
    }

    // 更新
    refresh() {
        let bbox = this.adapterBbox();
        let sites = this.randomSites();
        this.cells = this.voronoi.compute(sites, bbox).cells;

        this.cells.map((cell) => {
            let shapeDots = this.getShapeDots(cell);
            this.result.push(shapeDots);
        });
    }

    // 通过cell获取点
    getShapeDots(cell) {

        let dots = [];
        cell.halfedges.forEach((item) => {
            let edge = item.edge;

	// 这里需要去重
	    let point = new Phaser.Geom.Point(edge.va.x, edge.va.y)
            dots.push(point);
	    point = new Phaser.Geom.Point(edge.vb.x, edge.vb.y)
            dots.push(point);
        });

	return new Phaser.Geom.Polygon(dots);
    }

    // 固定区域生成随机点
    randomSites() {

        let sites = [];

        for (let i = 0; i < MIN_RANDOM_SITES; i++) {
            let tempSite = {
                x: this.getOneRandom(WIDTH),
                y: this.getOneRandom(HEIGHT)
            };
            sites.push(tempSite);
        }

        return sites;
    }

    // 在固定区间获取随机数
    getOneRandom(value) {
        let minVal = 3,
            maxVal = value - minVal;
        return minVal + Math.random() * maxVal;

    }

    // bbox适配器
    adapterBbox() {
        return {
            xl: 0,
            yt: 0,
            xr: WIDTH,
            yb: HEIGHT
        };
    }
}
export default Shape;
