import 'phaser';
import BootScene from './scenes/BootScene';
import GameScene from './scenes/GameScene';
import TitleScene from './scenes/TitleScene';


let canvas = document.querySelector('canvas');
// 初始化canvas
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

const config = {
    // For more settings see <https://github.com/photonstorm/phaser/blob/master/src/boot/Config.js>
    type: Phaser.CANVAS,
    canvas: canvas,
    pixelArt: true,
    parent: 'content',
    width: window.innerWidth,
    height: window.innerHeight,
    physics: {
	default: 'matter',
        matter: {
            gravity: {
                y: 0.3
            },
            debug: true
        }
	/*
        default: 'arcade',
        arcade: {
            gravity: {
                y: 800
            },
            debug: false
        }
	*/
    },
    scene: [
        BootScene,
        TitleScene,
        GameScene
    ]
};

window.onload = () => {
    window.game = new Phaser.Game(config);
}
