import Shape from '../helpers/Shape';

/*
 * 游戏
 * */
class GameScene extends Phaser.Scene {
    constructor(test) {
        super({
            key: 'GameScene'
        });

        this.shape = new Shape();
    }

    preload() {
        this.load.image('16x16sprites', 'assets/images/16x16sprites.png');

    }

    create() {

        this.matter.world.setBounds(0, 0, window.innerWidth, window.innerHeight);

        // 分数
        this.score = this.add.text(16, 16, `score: 1`, {
            fontSize: '32px',
            fill: '#fff'
        });

        //this.draw();
        this.input.keyboard.on('keyup_D', () => {
            this.draw();
        });
    }

    draw() {
        // 画形状
        this.polygon = this.shape.get();

        let pathes = this.polygon.points.map((item) => {
            return `${item.x} ${item.y}`;
        });

        pathes = pathes.join(' ');

        let polygon = this.matter.world.fromPath(pathes);

        var poly = this.matter.add.image(40, 40, 'orange', null, {
            shape: {
                type: 'fromVerts',
                verts: polygon
            }
        });

        poly.alpha = 0;
        poly.setBounce(1);
        poly.setFriction(0, 0, 0);

	this.matter.add.mouseSpring();
    }

    update(time, delta) {

    }
}

export default GameScene;
