/*
 * 游戏启动
 * */
class BootScene extends Phaser.Scene {
    constructor(test) {
        super({
            key: 'BootScene'
        });
    }
    preload() {
        // 初始化启动获取资源
        FBInstant.initializeAsync().then(() => {
            // Start loading game assets here
            console.log('start');
            //this._loadResources();
            var locale = FBInstant.getLocale(); // 'en_US'
            var platform = FBInstant.getPlatform(); // 'IOS'
            var sdkVersion = FBInstant.getSDKVersion(); // '3.0'
            var playerID = FBInstant.player.getID();

            console.log(locale, platform, sdkVersion, playerID);
        });
        this.load.on('progress', (value) => {
            console.log('progress', value);

            // 设置进度
            FBInstant.setLoadingProgress(value * 100);
        });

        // Register a load complete event to launch the title screen when all files are loaded
        this.load.on('complete', () => {
            // 关闭加载进度弹窗进入游戏
            this.scene.start('GameScene');
            FBInstant.startGameAsync().then(() => {
                this.scene.start('GameScene');
            });
        });

        this.load.image('background-clouds', 'assets/images/clouds.png');
        this.load.tilemapTiledJSON('map', 'assets/tilemaps/super-mario.json');

    }

}

export default BootScene;
